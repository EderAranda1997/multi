<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/create', 'MultimediaController@create')->name('create');
Route::post('/store', 'MultimediaController@store')->name('store');

Route::post('/check2', 'IaController@check2')->name('check2');

Route::get('/crear', 'IaController@crear')->name('crear');
Route::post('/guardar', 'IaController@guardar')->name('guardar');

Route::get('/buscar', 'MultimediaController@buscar')->name('buscar');
Route::get('/buscador', 'MultimediaController@buscador')->name('buscador');





<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Imagen</title>

</head>
<body>


<form action="" method="POST" class="padre">
    @csrf
    <div class="c-form">
        <div class="row-superior">
            <span class="superior">Entidad</span>
            <span class="superior">Descripción</span>
            <span class="superior">Puntuación</span>
        </div>

        @if(isset($recorridos['faces']['detectionConfidence']) === true)
            @foreach($recorridos['faces']['detectionConfidence'] as $score)
            <div class="row-internos">
                <span id='face' class="interior">Face</span>
                <span id='descripcion' class="interior">Persona</span>
                <span id='score' class="interior">{{ $score }}</span>
            </div>
            @endforeach
        @endif

        @if(isset($recorridos['faces']['emocion']['joyLikelihood']) === true)
            <div class="row-internos">
                <span id='face' class="interior">Face</span>
                <span id='descripcion' class="interior">Persona alegre</span>
                <span id='score' class="interior">{{ $recorridos['faces']['emocion']['joyLikelihood']  }}</span>
            </div>
        @endif
        @if(isset($recorridos['faces']['emocion']['sorrowLikelihood']) === true)
            <div class="row-internos">
                <span id='face' class="interior">Face</span>
                <span id='descripcion' class="interior">Persona triste</span>
                <span id='score' class="interior">{{ $recorridos['faces']['emocion']['sorrowLikelihood'] }}</span>
            </div>
        @endif
        @if(isset($recorridos['faces']['emocion']['angerLikelihood']) === true)
            <div class="row-internos">
                <span id='face' class="interior">Face</span>
                <span id='descripcion' class="interior">Persona enojada</span>
                <span id='score' class="interior">{{ $recorridos['faces']['emocion']['angerLikelihood'] }}</span>
            </div>
        @endif
        @if(isset($recorridos['faces']['emocion']['surpriseLikelihood']) === true)
            <div class="row-internos">
                <span id='face' class="interior">Face</span>
                <span id='descripcion' class="interior">Persona sorprendida</span>
                <span id='score' class="interior">{{ $recorridos['faces']['emocion']['surpriseLikelihood'] }}</span>
            </div>
        @endif

        @if(isset($recorridos['landmarks']) === true)
            @for($i = 0; $i < count($recorridos['landmarks']['description']); $i++)
            <div class="row-internos">
                <span id='face' class="interior">Landmarks</span>
                <span id='descripcion' class="interior">{{ $recorridos['landmarks']['description'][$i] }}</span>
                <span id='score' class="interior">{{ $recorridos['landmarks']['score'][$i] }}</span>
            </div>
            @endfor
        @endif

        @if(isset($recorridos['logos']) === true)
            @for($i = 0; $i < count($recorridos['logos']['description']); $i++)
                <div class="row-internos">
                    <span id='face' class="interior">Logos</span>
                    <span id='descripcion' class="interior">{{ $recorridos['logos']['description'][$i] }}</span>
                    <span id='score' class="interior">{{ $recorridos['logos']['score'][$i] }}</span>
                </div>
            @endfor
        @endif

        @if(isset($recorridos['text']) === true)
            <div class="row-internos">
                <span id='face' class="interior">Text</span>
                <span id='descripcion' class="interior">{{ $recorridos['text']['description'][0] }}</span>
                <span id='score' class="interior">0.95</span>
            </div>
        @endif

        @if(isset($recorridos['safeSearch']['contenido']['adult']) === true)
        <div class="row-internos">
            <span id='face' class="interior">SafeSearch</span>
            <span id='descripcion' class="interior">Contenido para adultos</span>
            <span id='score' class="interior">{{ $recorridos['safeSearch']['contenido']['adult'] }}</span>
        </div>
        @endif
        @if(isset($recorridos['safeSearch']['contenido']['spoof']) === true)
            <div class="row-internos">
                <span id='face' class="interior">SafeSearch</span>
                <span id='descripcion' class="interior">Contenido de parodia</span>
                <span id='score' class="interior">{{ $recorridos['safeSearch']['contenido']['spoof'] }}</span>
            </div>
        @endif
        @if(isset($recorridos['safeSearch']['contenido']['medical']) === true)
            <div class="row-internos">
                <span id='face' class="interior">SafeSearch</span>
                <span id='descripcion' class="interior">Contenido de medicina</span>
                <span id='score' class="interior">{{ $recorridos['safeSearch']['contenido']['medical'] }}</span>
            </div>
        @endif
        @if(isset($recorridos['safeSearch']['contenido']['racy']) === true)
            <div class="row-internos">
                <span id='face' class="interior">SafeSearch</span>
                <span id='descripcion' class="interior">Contenido picante</span>
                <span id='score' class="interior">{{ $recorridos['safeSearch']['contenido']['racy'] }}</span>
            </div>
        @endif
        @if(isset($recorridos['safeSearch']['contenido']['racy']) === true)
            @for($i = 0; $i < count($recorridos['imageProperties']['colors']); $i++)
                <div class="row-internos">
                    <span id='face' class="interior">ImageProperties</span>
                    <span id='descripcion' class="interior">{{ $recorridos['imageProperties']['colors'][$i] }}</span>
                    <span id='score' class="interior">{{ $recorridos['imageProperties']['score'][$i] }}</span>
                </div>
            @endfor
        @endif
        @if(isset($recorridos['web']) === true)
            @for($i = 0; $i < count($recorridos['web']['description']); $i++)
                <div class="row-internos">
                    <span id='face' class="interior">Web</span>
                    <span id='descripcion' class="interior">{{ $recorridos['web']['description'][$i] }}</span>
                    <span id='score' class="interior">{{ $recorridos['web']['score'][$i] }}</span>
                </div>
            @endfor
        @endif


    </div>
    <input type="submit" value="Enviar" class="btn-enviar">
</form>

<style>
    html{
        font-size: 16px
    }
    *{
        box-sizing:border-box;
        margin:0;
        padding:0
    }
    .padre {
        display: flex;
        flex-direction: row;
        position: absolute;
        left: 50%;
        transform: translateX(-50%);
        width: 80vw;
        min-height: 10rem;
        top: 2rem;
    }
    .c-form {
        display:flex;
        width: 100%;
        height: 100%;
        box-shadow: 0 8px 0 0 rgba(0,0,0,.3);
        box-shadow: 2px 8px 5px 0 rgba(0,0,0,.3);
        display:flex;
        flex-direction:column;
        border-radius: 5px;
        overflow: hidden;
    }
    .row-superior {
        background-color: rgba(0,191,255)
    }
    .row-superior,
    .row-internos {
        width: 100%;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
    }
    .row-internos:nth-child(2n+1){
        background-color: rgba(0,0,0,.1);
    }
    .superior {
        color: #fff
    }
    .superior,
    .interior {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
        width: calc(100% / 3);
        height: 2rem;
    }
    .btn-enviar {
        width: 10rem;
        height: 2rem;
        border-radius: 4px;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
        background-color: rgba(0,191,255);
        color: #fff;
        margin-left: 2rem;
        border: none;
    }
</style>
<script>
    let id = document.getElementById("score");
    console.log(id);
</script>
</body>
</html>

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileLabelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_label', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('file_id')->unsigned();
            $table->bigInteger('label_id')->unsigned();
            $table->string('content', 5000)->nullable();
            $table->float('score')->nullable();


            $table->foreign('file_id')->references('id')->on('files')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('label_id')->references('id')->on('labels')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_label');
    }
}

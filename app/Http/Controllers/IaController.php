<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use Google\Cloud\Vision\V1\Likelihood;
use Google\Cloud\Vision\V1\Feature\Type;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Google\Cloud\Vision\VisionClient;

use App\Label;
use App\File;
class IaController extends Controller
{
    public function crear()
    {
        return view('image.label');

    }

    public function guardar(Request $request)
    {

        $request->validate([
            'name' => 'required',
        ]);

        $label = Label::create([
            'name' => $request->name,
            'predecesor_id' => $request->predecesor_id,
        ]);

        return view('image.index');



    }

    public function check2 (Request $request){
        $request->validate([
            'image' => ['image'],
        ]);

        if($request->file('image')){
            $GLOBALS['path'] = $request->file('image')->store('public/galeryDesign');
            $GLOBALS['id'] = File::create([
                'original' => $GLOBALS['path'],
            ]);

            require __DIR__.'/../../../vendor/autoload.php';

            $vision = new VisionClient(['keyFile' => json_decode(file_get_contents(__DIR__.'/../../../key.json'), true)]);
            $familyPhotoResource = fopen(__DIR__.'/../../../public' . Storage::url($GLOBALS['path']), 'r');
            $image = $vision->image($familyPhotoResource,
                ['FACE_DETECTION',
                'WEB_DETECTION',
                'LABEL_DETECTION',
                'IMAGE_PROPERTIES',
                'TEXT_DETECTION',
                'SAFE_SEARCH_DETECTION',
                'LANDMARK_DETECTION',
                'LOGO_DETECTION',

            ]);

            $GLOBALS['datos'] = array();

            $result = $vision->annotate($image);

            if( $result->faces() !== null ){
                $faces = $result->faces();

                for ($i=0; $i < count($faces) ; $i++) {
                    $GLOBALS['datos']['Faces']['content'][] = "faces";
                    $GLOBALS['datos']['Faces']['score'][] = $faces[$i]->info()['detectionConfidence'];
                    $GLOBALS['emociones'] = array();
                    $GLOBALS['emociones'] = $faces[$i]->info();
                    $GLOBALS['filtrado'] = array();
                    $nombres = array_keys($GLOBALS['emociones']);
                    foreach ($nombres as $nombre) {
                        if($nombre === "joyLikelihood" or $nombre === "sorrowLikelihood" or $nombre === "angerLikelihood" or $nombre === "surpriseLikelihood")
                        {
                            if( $GLOBALS['emociones'][$nombre] === 'VERY_LIKELY'){
                                $GLOBALS['filtrado'][$nombre][] = 0.9;
                            }
                            if( $GLOBALS['emociones'][$nombre] === 'LIKELY'	){
                                $GLOBALS['filtrado'][$nombre][] = 0.7;
                            }
                            if( $GLOBALS['emociones'][$nombre] === 'POSSIBLE'	){
                                $GLOBALS['filtrado'][$nombre][] = 0.5;
                            }
                            if( $GLOBALS['emociones'][$nombre] === 'UNLIKELY'	){
                                $GLOBALS['filtrado'][$nombre][] = 0.3;
                            }
                            if( $GLOBALS['emociones'][$nombre] === 'VERY_UNLIKELY'){
                                $GLOBALS['filtrado'][$nombre][] = 0.1;
                            }
                            if( $GLOBALS['emociones'][$nombre] === 'UNKNOWN'	){
                                $GLOBALS['filtrado'][$nombre][] = 0;
                            }
                        }
                    }
                }
                $GLOBALS['datos']['Emotions']['content'][] = "cheerful, happy, smiling";
                $GLOBALS['datos']['Emotions']['content'][] = "sad, melancholic, crying";
                $GLOBALS['datos']['Emotions']['content'][] = "furious, angry";
                $GLOBALS['datos']['Emotions']['content'][] = "surprised";
                $GLOBALS['datos']['Emotions']['score'][] = max($GLOBALS['filtrado']['joyLikelihood']);
                $GLOBALS['datos']['Emotions']['score'][]= max($GLOBALS['filtrado']['sorrowLikelihood']);
                $GLOBALS['datos']['Emotions']['score'][] = max($GLOBALS['filtrado']['angerLikelihood']);
                $GLOBALS['datos']['Emotions']['score'][] = max($GLOBALS['filtrado']['surpriseLikelihood']);

            }


            if( $result->landmarks() !== null ){
                $landmarks = $result->landmarks();
                for ($i=0; $i < count($landmarks) ; $i++) {

                    if(isset($landmarks[$i]->info()['description']) === true){
                        $GLOBALS['datos']['Landmarks']['content'][] = $landmarks[$i]->info()['description'];
                    }
                    if(isset($landmarks[$i]->info()['description']) === false){
                        $GLOBALS['datos']['Landmarks']['content'][] = "null";
                    }
                    if(isset($landmarks[$i]->info()['score']) === true){
                        $GLOBALS['datos']['Landmarks']['score'][] = $landmarks[$i]->info()['score'];
                    }
                    if(isset($landmarks[$i]->info()['score']) === false){
                        $GLOBALS['datos']['Landmarks']['score'][] = 0.5;
                    }

                }
            }

            if( $result->logos() !== null ){
                $logos = $result->logos();
                for ($i=0; $i < count($logos) ; $i++) {
                    if(isset($logos[$i]->info()['description']) === true){
                        $GLOBALS['datos']['Logos']['content'][] = $logos[$i]->info()['description'];
                    }
                    if(isset($logos[$i]->info()['description']) === false){
                        $GLOBALS['datos']['Logos']['content'][] = "null";
                    }
                    if(isset($logos[$i]->info()['score']) === true){
                        $GLOBALS['datos']['Logos']['score'][] = $logos[$i]->info()['score'];
                    }
                    if(isset($logos[$i]->info()['score']) === false){
                        $GLOBALS['datos']['Logos']['score'][] = 0.5;
                    }
                }
            }

            if( $result->labels() !== null ){
                $labels = $result->labels();
                for ($i=0; $i < count($labels) ; $i++) {

                    if(isset($labels[$i]->info()['description']) === true){
                        $GLOBALS['datos']['Labels']['content'][] = $labels[$i]->info()['description'];
                    }
                    if(isset($labels[$i]->info()['description']) === false){
                        $GLOBALS['datos']['Labels']['content'][] = "null";
                    }
                    if(isset($labels[$i]->info()['score']) === true){
                        $GLOBALS['datos']['Labels']['score'][] =$labels[$i]->info()['score'];
                    }
                    if(isset($labels[$i]->info()['score']) === false){
                        $GLOBALS['datos']['Labels']['score'][] = 0.5;
                    }
                }
            }



            if( $result->text() !== null ){
                $text = $result->text();
                for ($i=0; $i < count($text); $i++) {
                    if(isset($text[$i]->info()['description']) === true){
                        $GLOBALS['datos']['Text']['content'][] = $text[$i]->info()['description'];
                        $GLOBALS['datos']['Text']['score'][] = 0.5;
                    }
                }
            }


            if( $result->safeSearch() !== null ){
                $safeSearch = $result->safeSearch();
                $GLOBALS['contenido'] = $safeSearch->info();

                $GLOBALS['filtro'] = array();
                $nombres = array_keys($GLOBALS['contenido']);

                foreach ($nombres as $nombre) {
                    if( $GLOBALS['contenido'][$nombre] === 'VERY_LIKELY'){
                        $GLOBALS['filtro'][$nombre][] = 0.9;
                    }
                    if( $GLOBALS['contenido'][$nombre] === 'LIKELY'	){
                        $GLOBALS['filtro'][$nombre][] = 0.7;
                    }
                    if( $GLOBALS['contenido'][$nombre] === 'POSSIBLE'	){
                        $GLOBALS['filtro'][$nombre][] = 0.5;
                    }
                    if( $GLOBALS['contenido'][$nombre] === 'UNLIKELY'	){
                        $GLOBALS['filtro'][$nombre][] = 0.3;
                    }
                    if( $GLOBALS['contenido'][$nombre] === 'VERY_UNLIKELY'){
                        $GLOBALS['filtro'][$nombre][] = 0.1;
                    }
                    if( $GLOBALS['contenido'][$nombre] === 'UNKNOWN'	){
                        $GLOBALS['filtro'][$nombre][] = 0;
                    }

                }
                $GLOBALS['datos']['Type of content']['content'][] = "desnudos";
                $GLOBALS['datos']['Type of content']['content'][] = "parodia";
                $GLOBALS['datos']['Type of content']['content'][] = "medicina";
                $GLOBALS['datos']['Type of content']['content'][] = "violencia";
                $GLOBALS['datos']['Type of content']['content'][] = "desnudos";
                $GLOBALS['datos']['Type of content']['score'][] = max( $GLOBALS['filtro']['adult']);
                $GLOBALS['datos']['Type of content']['score'][] = max( $GLOBALS['filtro']['spoof']);
                $GLOBALS['datos']['Type of content']['score'][] = max( $GLOBALS['filtro']['medical']);
                $GLOBALS['datos']['Type of content']['score'][] = max( $GLOBALS['filtro']['violence']);
                $GLOBALS['datos']['Type of content']['score'][] = max( $GLOBALS['filtro']['racy']);

            }


            if( $result->imageProperties() !== null ){
                $imageProperties = $result->imageProperties();
                for ($i=0; $i < count($imageProperties->info()['dominantColors']['colors']) ; $i++) {

                    $GLOBALS['red'] = '';
                    $GLOBALS['green'] = '';
                    $GLOBALS['blue'] = '';
                    $GLOBALS['alpha'] = '';

                    if(isset($imageProperties->info()['dominantColors']['colors'][$i]['color']['red']) === true ){
                        $GLOBALS['red'] = $imageProperties->info()['dominantColors']['colors'][$i]['color']['red'];
                    }
                    if(isset($imageProperties->info()['dominantColors']['colors'][$i]['color']['red']) === false){
                        $GLOBALS['red'] = 0;
                    }
                    if(isset($imageProperties->info()['dominantColors']['colors'][$i]['color']['green']) === true ){
                        $GLOBALS['green'] = $imageProperties->info()['dominantColors']['colors'][$i]['color']['green'];
                    }
                    if(isset($imageProperties->info()['dominantColors']['colors'][$i]['color']['green']) === false){
                        $GLOBALS['green'] = 0;
                    }
                    if(isset($imageProperties->info()['dominantColors']['colors'][$i]['color']['blue']) === true ){
                        $GLOBALS['blue'] = $imageProperties->info()['dominantColors']['colors'][$i]['color']['blue'];
                    }
                    if(isset($imageProperties->info()['dominantColors']['colors'][$i]['color']['blue']) === false){
                        $GLOBALS['blue'] = 0;
                    }

                    if(isset($imageProperties->info()['dominantColors']['colors'][$i]['color']['alpha']) === true){
                        $GLOBALS['alpha'] = $imageProperties->info()['dominantColors']['colors'][$i]['color']['alpha'];
                    }
                    if(isset($imageProperties->info()['dominantColors']['colors'][$i]['color']['alpha']) === false){
                        $GLOBALS['alpha'] = 1;
                    }


                    $GLOBALS['datos']['Colors']['content'][] = "rgba" . "(" . $GLOBALS['red']. "," . $GLOBALS['green'] . "," .  $GLOBALS['blue'] . "," . $GLOBALS['alpha']. ")";
                    $GLOBALS['datos']['Colors']['score'][] = $imageProperties->info()['dominantColors']['colors'][$i]['score'];

                }
            }

            if( $result->web() !== null ){
                $web = $result->web();
                for ($i=0; $i < count($web->entities()) ; $i++) {
                    if(isset($web->entities()[$i]->info()['description']) === true){
                        $GLOBALS['datos']['Entities']['content'][] = $web->entities()[$i]->info()['description'];
                    }
                    if(isset($web->entities()[$i]->info()['description']) === false){
                        $GLOBALS['datos']['Entities']['content'][] = 'null';
                    }
                    if(isset($web->entities()[$i]->info()['score']) === true){
                        $GLOBALS['datos']['Entities']['score'][] = $web->entities()[$i]->info()['score'];
                    if(isset($web->entities()[$i]->info()['score']) === false){
                        $GLOBALS['datos']['Entities']['score'][] = 0.5;
                    }

                }
            }

            $GLOBALS['nombres'] = array_keys($GLOBALS['datos']);
            $GLOBALS['labels'] = Label::all();

            foreach ($GLOBALS['nombres'] as $nombre) {
                $label_id = $GLOBALS['labels'][0]->where('name', $nombre)->get()[0]->id;
                $imagen= File::find($GLOBALS['id']->id);
                for ($i=0; $i < count($GLOBALS['datos'][$nombre]['content']); $i++) {
                    $imagen->labels()->attach($label_id,['content'=> $GLOBALS['datos'][$nombre]['content'][$i], 'score'=>$GLOBALS['datos'][$nombre]['score'][$i]]);
                }
            }

            $recorridos = $GLOBALS['datos'];
            dd($recorridos);

            return view('image.mostrar', compact('recorridos'));

            }

        }
    }
}

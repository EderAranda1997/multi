<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\File;
use App\FileLabel;
use Intervention\Image\Facades\Image;

class MultimediaController extends Controller
{
    public function create()
    {
        return view('image.index');
    }


    public function store(Request $request)
    {
        $request->validate([
            'image' => ['image'],
        ]);

        if($request->file('image')){
            $GLOBALS['path'] = $request->file('image')->store('public/galeryDesign');
        }
        return redirect('check2');
    }

    public function buscar()
    {
        return view('image.buscar');
    }
    public function buscador(Request $request)
    {
        $words = explode(" ", $request->content);
        $GLOBALS['files'] = File::all();
        $GLOBALS['contents'] = array();
        $GLOBALS['order'] = array();
        $GLOBALS['keys'] = array();
        $GLOBALS['filtrado_1'] = array();
        $GLOBALS['filtrado_2'] = array();


        for ($i=0; $i < count($words) ; $i++) {
            $GLOBALS['contents'][] = FileLabel::where('content','LIKE',"%$words[$i]%")->get();
            for ($j=0 ; $j < count($GLOBALS['contents'][$i]) ; $j++ ) {
                $GLOBALS['order'][] = $GLOBALS['contents'][$i][$j];
            }
        }
        for ($k=0; $k<count($GLOBALS['order']); $k++) {

            foreach ($GLOBALS['files'] as $file) {
                if($file->id === $GLOBALS['order'][$k]->file_id)
                {
                    $GLOBALS['filtrado_1'][$file->id]['score'][] = $GLOBALS['order'][$k]->score;
                }
            }
        }

        $GLOBALS['keys'] = array_keys($GLOBALS['filtrado_1']);
        foreach ($GLOBALS['keys'] as $key) {
            $GLOBALS['filtrado_2'][$key] = array_sum($GLOBALS['filtrado_1'][$key]['score']);
        }
        arsort($GLOBALS['filtrado_2']);

        dd($GLOBALS['filtrado_2']);








        return view('image.exito');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\File;

class Label extends Model
{
    protected $fillable = [
        'id', 'name', 'predecesor_id'
    ];

    public function images(){
        return $this->belongsToMany(File::class)->withTimestamps();
    }


    public function labels(){
        return $this->hasOne(Label::class, 'predecesor_id', 'id');
    }

    public function labels2(){
        return $this->hasMany(Label::class, 'id', 'predecesor_id');
    }
}

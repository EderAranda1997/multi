<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Label;

class File extends Model
{
    protected $fillable = [
        'id', 'original', 'size_64px', 'size_256px'
    ];
    public function labels(){
        return $this->belongsToMany(Label::class)->withTimestamps();
    }



}

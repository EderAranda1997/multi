<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;
use App\Label;
use App\File;


class FileLabel extends Pivot
{
    protected $fillable = [
        'id', 'file_id', 'label_id', 'content', 'score'
    ];
    public function files(){
        return $this->hasOne(File::class);
    }
    public function labels(){
        return $this->hasOne(Label::class);
    }

}
